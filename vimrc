" @TODO: CHECK 2021:
" https://github.com/YashdalfTheGray/dotfiles/blob/master/macos/.vimrc
" https://vimawesome.com/
" -----------------------------------------------------------------------------
" |   H E L P    F O L D I N G                                                |{{{
" -----------------------------------------------------------------------------
"  Create fold: visual select what you want, then hit: zf
"  Delete folde: go to folded line then hit: zd
"  zo: open fold
"  zc: close fold
"  za: toggle fold
"  zO, zC, zA: same as above, but for all
"  zr: open all folds
"  zm: close all folds

" -----------------------------------------------------------------------------}}}
" |   H E L P    B U F F E R S                                                |{{{
" -----------------------------------------------------------------------------
"  :bd          - deletes the current buffer, error if there are unwritten changes
"  :bd!         - deletes the current buffer, no error if unwritten changes
"  :bufdo bd    - deletes all buffers, stops at first error (unwritten changes)
"  :bufdo! bd   - deletes all buffers except those with unwritten changes
"  :bufdo! bd!  - deletes all buffers, no error on any unwritten changes
"
"  :bw          - completely deletes the current buffer, error if there are unwritten changes
"  :bw!         - completely deletes the current buffer, no error if unwritten changes
"  :bufdo bw    - completely deletes all buffers, stops at first error (unwritten changes)
"  :bufdo! bw   - completely deletes all buffers except those with unwritten changes
"  :bufdo! bw!  - completely deletes all buffers, no error on any unwritten changes
"
"  :set confirm - confirm changes (Yes, No, Cancel) instead of error
"  Here are a few other useful buffer commands:
"
"  :ls          - list open buffers
"  :b N         - open buffer number N (as shown in ls)
"  :tabe +Nbuf  - open buffer number N in new tab
"  :bnext       - go to the next buffer (:bn also)
"  :bprevious   - go to the previous buffer (:bp also)

" -----------------------------------------------------------------------------}}}
" |   H E L P    R E Q U I R E M E N T S                                      |{{{
" -----------------------------------------------------------------------------
"   __ALL__ {{{
"       Fedora:
"           dnf install cmake gcc-c++ make yarnpkg python3-flake8 python3-devel mono-complete golang nodejs npm java-17-openjdk-headless java-17-openjdk-devel
"       Ubuntu:
"           apt install build-essential python3-dev cmake yarnpkg python3-flake8 mono-complete golang nodejs npm openjdk-17-jdk openjdk-17-jre
"       MacOSX:
"           brew install vim cmake go nodejs java yarn flake8
"           sudo ln -sfn $(brew --prefix java)/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
"       Windows:
"           rm -rf WINDOWS && install Linux || FreeBSD
"
"       [ ! -d ~/opt ] && mkdir ~/opt ; cd ~/opt
"       git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
"       cd nerd-fonts && ./install.sh
"       * Then use one of their fonts in terminal.
"}}}
"   DevIcons: (ryanoasis/vim-devicons) {{{
"       Needs the Nerd-Fonts:
"           [ ! -d ~/opt ] && mkdir ~/opt ; cd ~/opt
"           git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
"           cd nerd-fonts && ./install.sh
"       Then use one of their fonts in terminal.
"}}}
"   YouCompleteMe: (ycm-core/YouCompleteMe) {{{
"       Fedora:
"           dnf install cmake gcc-c++ make python3-devel
"           Install mono-complete, go, node and npm
"               dnf install mono-complete golang nodejs npm
"           Install java:
"               dnf install java-17-openjdk java-17-openjdk-devel
"               OR
"               dnf install java-17-openjdk-headless java-17-openjdk-devel
"       Ubuntu:
"           Install CMake, Vim and Python
"               apt install build-essential python3-dev cmake
"           * Ubuntu LTS
"               apt install build-essential python3-dev cmake3
"           Install mono-complete, go, node, java and npm
"               apt install mono-complete golang nodejs npm openjdk-17-jdk openjdk-17-jre
"       MacOSX:
"           brew install vim cmake go nodejs java
"           sudo ln -sfn $(brew --prefix java)/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
"}}}
"   coc.nvim: (neoclide/coc.nvim) {{{
"       Fedora:
"           dnf install yarnpkg
"       Ubuntu:
"           apt install yarnpkg
"       MacOSX:
"           brew install yarn
"}}}
"   vim-flake8 (nvie/vim-flake8) {{{
"       Fedora:
"           dnf install python3-flake8
"       Ubuntu:
"           apt install flake8
"       MacOSX:
"           brew install flake8
"}}}
"   markdown-preview.nvim:   (iamcco/markdown-preview.nvim) {{{
"       Fedora:
"           dnf install yarnpkg
"       Ubuntu:
"           apt install yarnpkg
"       MacOSX:
"           brew install yarn
"}}}
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------
" |   O S      D E T E C T I O N                                              |{{{
" -----------------------------------------------------------------------------
"  see: https://vi.stackexchange.com/questions/2572/detect-os-in-vimscript
if !exists("g:os")
    if has("win64") || has("win32") || has("win16") || has("win95") || has("win32unix")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
        " Darwin
        " Linux
        " FreeBSD
    endif
endif
" -----------------------------------------------------------------------------}}}
" |   C P U    A R C H I T E C T U R E                                        |{{{
" -----------------------------------------------------------------------------
" Function to detect the CPU architecture
function! GetCpuArch()
    if has("win64") || has("win32") || has("win16") || has("win95") || has("win32unix")
        let l:arch = substitute(system('wmic os get osarchitecture'), '\n', '', '')
        " Clean up the output to just get the architecture part
        let l:arch = substitute(l:arch, '\r', '', '') " Remove carriage return
        let l:arch = substitute(l:arch, '^.*\(\(32-bit\|64-bit\)\)$', '\1', '')
        " 64-bit
        " 32-bit
    else
        let l:arch = substitute(system('uname -m'), '\n', '', '')
        " on Linux
            " x86_64
            " arm64
            " i686
        " on Darwin
            " arm64
            " x86_64
        " on FreeBSD
            " amd64
            " i386
    endif
    return l:arch
endfunction

" Set the global variable g:arch if it doesn't exist
if !exists("g:arch")
    let g:arch = GetCpuArch()
endif

" Example of using the detected architecture
"if g:arch == 'arm64'
"    " Settings for ARM architecture
"    set tabstop=4
"elseif g:arch == 'x86_64'
"    " Settings for x86_64 architecture
"    set tabstop=8
"else
"    " Default settings for other architectures
"    set tabstop=2
"endif

" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------
" |   P L U G I N S                                                           |{{{
" -----------------------------------------------------------------------------
" |   [PLUGIN]: Vim-Plug.................(junegunn/vim-plug)                 |{{{
" -----------------------------------------------------------------------------
" https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')
" Make sure you use single quotes

" [ Integration       ] {{{
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' } " NERDTree
Plug 'preservim/tagbar', {'on': 'TagbarToggle'}         " TagBar (CTags)
Plug 'kien/ctrlp.vim'       " Full path fuzzy file, buffer, mru, tag, ... finder for Vim.
Plug 'tpope/vim-fugitive'   " Git Integration
Plug 'wesQ3/vim-windowswap'   " Window Swap
"}}}
" [ Python            ] {{{
Plug 'nvie/vim-flake8', { 'for': 'python' }                 " Python Syntax Checker
Plug 'tmhedberg/SimpylFold', { 'for': 'python' }            " Python Folding
Plug 'davidhalter/jedi-vim', { 'for': 'python' }            " The Awesome Python Autocompletion
Plug 'vim-scripts/indentpython.vim', { 'for': 'python' }    " Python Indent
"}}}
" [ Markdown          ] {{{
"Plug 'suan/vim-instant-markdown', { 'for': 'markdown' }    " Instant Markdown previews from Vim
" Markdown previews from Vim
" If you don't have nodejs and yarn, use pre build
"   { 'do': { -> mkdp#util#install() } }
" If you have nodejs and yarn
"   { 'do': 'cd app & yarn install' }
Plug 'godlygeek/tabular' | Plug 'iamcco/markdown-preview.nvim', { 'for': 'markdown', 'do': { -> mkdp#util#install() } }
"}}}
" [ Writing           ] {{{
"Plug 'junegunn/limelight.vim'   " Focus on current paragraph by diming the rest
Plug 'junegunn/goyo.vim'        " Remove all distraction, and centering the text
"}}}
" [ Languages         ] {{{
"Plug 'scrooloose/syntastic'     " Syntax Checker
"Plugin ‘dense-analysis/ale’    " Syntax checking/lint
"Plug 'ap/vim-css-color'        " CSS Color
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }   " Markdown Syntax
Plug 'smancill/conky-syntax.vim', { 'for': 'conky' }    " Conky Syntax
Plug 'lifepillar/pgsql.vim', { 'for': ['sql', 'mysql', 'pgsql'] }   " PostgreSQL Syntax
"}}}
" [ Text Manipulation ] {{{
Plug 'tpope/vim-surround'       " Surrounding: ysw)
Plug 'tpope/vim-commentary'     " Comment stuff out
"Plug 'mg979/vim-visual-multi', {'branch': 'master'}    " Vim Multi Cursors
"}}}
" [ Completion        ] {{{
"Plug 'https://github.com/neoclide/coc.nvim', {'do': 'yarn install'}    " Custom popup menu with snippet support
" A code-completion engine for Vim
if g:os == "Linux"
    Plug 'ycm-core/YouCompleteMe', { 'as':  'YouCompleteMe.lin', 'do': 'python3 install.py --all' }
elseif g:os == "FreeBSD"
    Plug 'ycm-core/YouCompleteMe', { 'as':  'YouCompleteMe.fbsd', 'do': 'python3 install.py --all' }
elseif g:os == "Darwin"
    if g:arch == 'arm64'
        Plug 'ycm-core/YouCompleteMe', { 'as':  'YouCompleteMe.mac-arm64', 'do': 'python3 install.py --all' }
    elseif g:arch == 'x86_64'
        Plug 'ycm-core/YouCompleteMe', { 'as':  'YouCompleteMe.mac-x86_64', 'do': 'python3 install.py --all' }
    endif
endif
"}}}
" [ Snippets          ] {{{
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
"}}}
" [ Colorschemes      ] {{{
Plug 'flazz/vim-colorschemes'               " Vim Colorschemes
Plug 'rafi/awesome-vim-colorschemes'        " Awesome Vim Colorschemes
Plug 'altercation/vim-colors-solarized'     " Solarized Colorscheme
Plug 'jnurmine/Zenburn'                     " Zenburn Colorscheme
" Make gvim-only colorschemes work transparently in terminal vim
"Plug 'godlygeek/csapprox'
"}}}
" [ Interface         ] {{{
Plug 'vim-airline/vim-airline'              " Airline
Plug 'vim-airline/vim-airline-themes'       " Airline Themes
Plug 'powerline/powerline'                  " Powerline
Plug 'powerline/fonts'                      " Powerline Fonts
Plug 'ryanoasis/vim-devicons'               " DevIcons
"}}}
" [ Misc              ] {{{
"Plug 'vimwiki/vimwiki'     " Personal Wiki for Vim
"}}}
" [ Still to add      ] {{{
" NEW (20221101_
"Plugin ‘python-mode/pymode.vim’    " Python mode
"
" https://dev.to/jones268/use-vim-as-a-python-ide-31e6
"
" https://github.com/python-mode/python-mode
" https://rapphil.github.io/vim-python-ide/
"
" https://github.com/ycm-core/YouCompleteMe
" https://github.com/junegunn/vim-plug
"
" https://github.com/dkarter/bullets.vim
" https://github.com/mbbill/undotree
" https://github.com/tpope/vim-dadbod/tree/master/autoload/db
" }}}

" Add plugins to &runtimepath
call plug#end()

" -----------------------------------------------------------------------------}}}
" |                       ENABLED PLUGINS                                    |{{{
" -----------------------------------------------------------------------------
" |   [PLUGIN]: NERDTree.................(scrooloose/nerdTree)                |{{{
" -----------------------------------------------------------------------------
" User instead of Netrw when doing an edit /foobar
let NERDTreeHijackNetrw=1
" Single click for everything
let NERDTreeMouseMode=1
let g:NERDTreeMouseMode = 2
let g:NERDTreeWinSize = 40
"let g:NERDTreeChDirMode=2

" Open NERDTree automatically when vim starts up
"autocmd vimenter * NERDTree

" Open a NERDTree automatically when vim starts up if no files were specified
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Changing default arrows
"let g:NERDTreeDirArrowExpandable = '▸'
"let g:NERDTreeDirArrowCollapsible = '▾'

" Hide .pyc files
"let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: JEDI-Vim.................(davidhalter/jedi-vim)               |{{{
" -----------------------------------------------------------------------------
" Awesome Python autocompletion with VIM
let g:jedi#show_call_signatures = 2 " solve undo-history bug: https://github.com/davidhalter/jedi-vim/issues/313
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: YouCompleteMe............(ycm-core/YouCompleteMe)             |{{{
" -----------------------------------------------------------------------------
" Help: {{{
"   Install on Linux: {{{
"       APT {{{
"           Install CMake, Vim and Python
"               apt install build-essential python3-dev cmake
"               * Ubuntu LTS
"                   apt install build-essential python3-dev cmake3
"           Install mono-complete, go, node, java and npm
"               apt install mono-complete golang nodejs npm openjdk-17-jdk openjdk-17-jre
"       }}}
"       DNF {{{
"           dnf install cmake gcc-c++ make python3-devel
"           Install mono-complete, go, node and npm
"               dnf install python3-devel mono-complete golang nodejs npm
"           Install java:
"               dnf install java-17-openjdk java-17-openjdk-devel
"               OR
"               dnf install java-17-openjdk-headless java-17-openjdk-devel
"       }}}
"   }}}
"   Install on MacOSX: {{{
"       brew install vim cmake go nodejs java
"       sudo ln -sfn $(brew --prefix java)/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
"       NOTE: {{{
"           If you want C-family completion, you MUST have the latest Xcode
"           installed along with the latest Command Line Tools (they are
"           installed automatically when you run clang for the first time, or
"           manually by running xcode-select --install)
"       }}}
"   }}}
"   Compile YCM {{{
"       For Intel and arm64 Macs, the bundled libclang/clangd work:
"           cd ~/.vim/bundle/YouCompleteMe
"           python3 install.py --all
"       If you have troubles with finding system frameworks or C++
"       standard library, try using the homebrew llvm:
"           brew install llvm
"           cd ~/.vim/bundle/YouCompleteMe
"           python3 install.py --system-libclang --all
"       Compiling YCM with semantic support for C-family languages through
"       clangd:
"           cd ~/.vim/bundle/YouCompleteMe
"           ./install.py --clangd-completer
"       Compiling YCM without semantic support for C-family languages:
"           cd ~/.vim/bundle/YouCompleteMe
"           ./install.py
"       And edit your vimrc to add the following line to use the Homebrew
"       llvm's clangd:
"           " Use homebrew's clangd
"           let g:ycm_clangd_binary_path = trim(system('brew --prefix llvm')).'/bin/clangd'
"   }}}
"   Remember: {{{
"       YCM is a plugin with a compiled component. If you update YCM using
"       Vundle and the ycm_core library APIs have changed (happens rarely),
"       YCM will notify you to recompile it. You should then rerun the install
"       process.
"   }}}
"   The following additional language support options are available: {{{
"       *   C# support: install by downloading the Mono macOS package and add
"           --cs-completer when calling install.py.
"       *   Go support: install Go and add --go-completer when calling 
"           install.py.
"       *   JavaScript and TypeScript support: install Node.js and npm and add
"           --ts-completer when calling install.py.
"       *   Rust support: add --rust-completer when calling install.py.
"       *   Java support: install JDK 17 and add --java-completer when calling
"           install.py.
"       *   To simply compile with everything enabled, there's a --all flag.
"           So, to install with all language features, ensure 
"           xbuild, go, node and npm tools are installed and in your PATH,
"           then simply run:
"               cd ~/.vim/bundle/YouCompleteMe
"               ./install.py --all
"   }}}
"}}}

" Use homebrew's clangd (MacOSX)
" let g:ycm_clangd_binary_path = trim(system('brew --prefix llvm')).'/bin/clangd'

" Semantic highlighting
let g:ycm_enable_semantic_highlighting=1
" or by buffer by setting: b:ycm_enable_semantic_highlighting=1

" Inlay hints
let g:ycm_enable_inlay_hints=1
" or by buffer by setting: b:ycm_enable_inlay_hints=1
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: UltiSnips................(ycm-core/YouCompleteMe)             |{{{
" -----------------------------------------------------------------------------
" Use tab to trigger snippet expansion
"let g:UltiSnipsExpandTrigger="<tab>"
" Enter
"let g:UltiSnipsExpandTrigger="<CR>"
let g:UltiSnipsExpandTrigger="<c-l>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Use <c-j> to jump forward in snippets
let g:UltiSnipsJumpForwardTrigger="<c-j>"
"let g:UltiSnipsJumpForwardTrigger="<c-n>"

" Use <c-k> to jump backward in snippets
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
"let g:UltiSnipsJumpBackwardTrigger="<c-p>"
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: Vim-Snippets.............(ycm-core/YouCompleteMe)             |{{{
" -----------------------------------------------------------------------------
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: coc.nvim.................(neoclide/coc.nvim)                  |{{{
" -----------------------------------------------------------------------------
" Help: Install {{{
"   Fedora:
"       dnf install yarnpkg
"   Ubuntu:
"       apt install yarnpkg
"   MacOSX:
"       brew install yarn
"}}}
" --- Just Some Notes ---
" :PlugClean :PlugInstall :UpdateRemotePlugins
"
" :CocInstall coc-python
" :CocInstall coc-clangd
" :CocInstall coc-snippets
" :CocCommand snippets.edit... FOR EACH FILE TYPE
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: SimpylFold...............(tmhedberg/SimpylFold)               |{{{
" -----------------------------------------------------------------------------
"autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
"autocmd BufWinLeave *.py setlocal foldexpr< foldmethod<

"  enable previewing of your folded classes' and functions' docstrings in the fold text
"let g:SimpylFold_docstring_preview = 1

" Want to see the docstrings for folded cod
"let g:SimpylFold_docstring_preview = 1

" don't want to see your docstrings folded
"let g:SimpylFold_fold_docstring = 0

" don't want to see your imports folded, add this
"let g:SimpylFold_fold_import = 0
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: vim-flake8...............(nvie/vim-flake8)                    |{{{
" -----------------------------------------------------------------------------
" Requirements:
"   dnf install python3-flake8
"
" Make your python code look pretty
let python_highlight_all=1
let syntastic_python_checkers=['flake8']
"let g:syntastic_python_checkers=['flake8']
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: Vim-DevIcons.............(ryanoasis/vim-devicons)             |{{{
" -----------------------------------------------------------------------------
let g:webdevicons_enable = 1            " loading the plugin
" Integration
let g:webdevicons_enable_nerdtree = 1   " NERDTree
let g:webdevicons_enable_unite = 1      " Unite
let g:webdevicons_enable_denite = 1     " Denite
let g:webdevicons_enable_vimfiler = 1   " Vimfiler
let g:webdevicons_enable_ctrlp = 1      " CtrlP
let g:webdevicons_enable_startify = 1   " Startify
let g:webdevicons_enable_airline_tabline = 1        " Airline
let g:webdevicons_enable_airline_statusline = 1     " Airline
let g:webdevicons_enable_flagship_statusline = 1    " Flagship
" Extra configs
" turn on/off file node glyph decorations (not particularly useful)
let g:WebDevIconsUnicodeDecorateFileNodes = 1

" use double-width(1) or single-width(0) glyphs
" only manipulates padding, has no effect on terminal or set(guifont) font
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

" whether or not to show the nerdtree brackets around flags
let g:webdevicons_conceal_nerdtree_brackets = 1

" the amount of space to use after the glyph character (default ' ')
let g:WebDevIconsNerdTreeAfterGlyphPadding = '  '

" Force extra padding in NERDTree so that the filetype icons line up vertically
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1

" the amount of space to use after the glyph character in vim-airline tabline(default '')
let g:WebDevIconsTabAirLineAfterGlyphPadding = ' '
" the amount of space to use before the glyph character in vim-airline tabline(default ' ')
let g:WebDevIconsTabAirLineBeforeGlyphPadding = ' '
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: TagBar...................(preservim/tagbar)                   |{{{
" -----------------------------------------------------------------------------
" For help
"   :helptags
"   :help tagbar
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: Vim-Airline..............(vim-airline/vim-airline)            |{{{
" -----------------------------------------------------------------------------
let g:airline_powerline_fonts = 1
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'
set t_Co=256 " to be colorful in tmux

" airline symbols
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
"let g:airline_symbols.space = "\ua0"
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: CtrlP.vim................(kien/ctrlp.vim)                     |{{{
" -----------------------------------------------------------------------------
" * Press <F5> to purge the cache for the current directory to get new 
"   files, remove deleted files and apply new ignore options.
" * Press <c-f> and <c-b> to cycle between modes.
" * Press <c-d> to switch to filename only search instead of full path.
" * Press <c-r> to switch to regexp mode.
" * Use <c-j>, <c-k> or the arrow keys to navigate the result list.
" * Use <c-t> or <c-v>, <c-x> to open the selected entry in a new tab or 
"   in a new split.
" * Use <c-n>, <c-p> to select the next/previous string in the prompt's 
"   history.
" * Use <c-y> to create a new file and its parent directories.
" * Use <c-z> to mark/unmark multiple files and <c-o> to open them.
"
" * Submit two dots '..' to go upward the directory tree by 1 level. To go
"   up multiple levels, use one extra dot for each extra level:
"   Raw input    Interpreted as
"   ..           ../
"   ...          ../../
"   ....         ../../../

" Exclude files and directories
"set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.tar,*.gz     " MacOSX/Linux
set wildignore+=*.so,*.swp,*.zip,*.tar,*.gz     " MacOSX/Linux
set wildignore+=*.bkp,*.dev                     " Dev excludes
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'

" 'c' - the directory of the current file.
" 'r' - the nearest ancestor that contains one of these directories or files:
"       .git .hg .svn .bzr
" 'a' - like c, but only if the current working directory outside of CtrlP is
"       not a direct ancestor of the directory of the current file.
" 'w' - begin finding a root from the current working directory outside of
"       CtrlP instead of from the directory of the current file (default).
"       Only applies when "r" is also present.
" 0 or '' (empty string) - disable this feature.
let g:ctrlp_working_path_mode = 'ra'

" Set this to 0 to enable cross-session caching by not deleting the cache files upon exiting Vim:
let g:ctrlp_clear_cache_on_exit = 1     " default: 1

" Set the directory to store the cache files:
let g:ctrlp_cache_dir = $HOME.'/.vim/files/cache/ctrlp'


let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(pyc|o|swp|exe|so|dll|zip|tar|gz)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

" Use a custom file listing command:
"let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux

map <C-S-p>    :CtrlPBuffer<CR>
map <C-[>    :CtrlPBuffer<CR>
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: markdown-preview.nvim....(iamcco/markdown-preview.nvim)       |{{{
" -----------------------------------------------------------------------------
"  CONFIG {{{
" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {}
    \ }

" use a custom markdown style must be absolute path
let g:mkdp_markdown_css = $HOME.'/.vim/files/inc/github-markdown.css'
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
let g:mkdp_highlight_css = ''

" use a custom port to start server or random for empty
let g:mkdp_port = ''

" preview page title
" ${name} will be replace with the file name
let g:mkdp_page_title = '「${name}」'
" }}}
" COMMANDS {{{
" Start the preview
" :MarkdownPreview
"
" Stop the preview"
" :MarkdownPreviewStop
" }}}
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: PgSQL.vim................(lifepillar/pgsql.vim)               |{{{
" -----------------------------------------------------------------------------
" Files with a .pgsql suffix are highlighted out of the box.
" If you want to highlight .sql files using this plugin by default
let g:sql_type_default = 'pgsql'

" Alternatively, after loading a .sql file use this command:
"   :SQLSetType pgsql.vim

" To set the file type in new buffers use:
":let b:sql_type_override='pgsql' | set ft=sql
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: WindowSwap.vim...........(wesQ3/vim-windowswap)               |{{{
" -----------------------------------------------------------------------------
let g:windowswap_map_keys = 0 "prevent default bindings
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------}}}
" |                       DISABLED PLUGINS                                   |{{{
" -----------------------------------------------------------------------------
" | - [PLUGIN]: Syntastic................(scrooloose/syntastic)               |{{{
" -----------------------------------------------------------------------------
"   Plug 'scrooloose/syntastic'     " Syntax Checker
"
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"let syntastic_stl_format = '[Syntax: %E{line:%fe }%W{#W:%w}%B{ }%E{#E:%e}]'
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: LimeLight.vim............(junegunn/limelight.vim)             |{{{
" -----------------------------------------------------------------------------
" Color name (:help cterm-colors) or ANSI code
"let g:limelight_conceal_ctermfg = 'gray'
"let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
"let g:limelight_conceal_guifg = 'DarkGray'
"let g:limelight_conceal_guifg = '#777777'

" Default: 0.5
"let g:limelight_default_coefficient = 0.7

" Number of preceding/following paragraphs to include (default: 0)
"let g:limelight_paragraph_span = 1

" Beginning/end of paragraph
"   When there's no empty line between the paragraphs
"   and each paragraph starts with indentation
"let g:limelight_bop = '^\s'
"let g:limelight_eop = '\ze\n^\s'

" Highlighting priority (default: 10)
"   Set it to -1 not to overrule hlsearch
"let g:limelight_priority = -1

" Goyo.vim integration
"autocmd! User GoyoEnter Limelight
"autocmd! User GoyoLeave Limelight!
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: vimwiki..................(vimwiki/vimwiki)                    |{{{
" -----------------------------------------------------------------------------
" helppage -> :h vimwiki-syntax
"set nocompatible
" vimwiki with markdown support
"let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: vim-instant-markdown.....(suan/vim-instant-markdown)          |{{{
" -----------------------------------------------------------------------------
let g:instant_markdown_autostart = 0    " disable autostart
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: Omni Completion..........(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
set wildmode=longest:full,full " make cmdline tab completion better
"set wildmode=list:longest   " make cmdline tab completion similar to bash
set wildmenu                " enable ctrl-n and ctrl-p to scroll thru matches
"set wildignore=*.o,*.obj,*~ " stuff to ignore when tab completing
"set omnifunc=syntaxcomplete#Complete

autocmd FileType html :set omnifunc=htmlcomplete#CompleteTags
"autocmd FileType python set omnifunc=pythoncomplete#Complete    " using jedi-vim instead
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete
"autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete " may require ruby compiled in //DEADSOUL
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: OmniCppComplete..........(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
"set nocp

" Configure tags - add additional tags here or comment out not-used ones
"set tags+=~/.vim/tags/stl
"set tags+=~/.vim/tags/gl
"set tags+=~/.vim/tags/sdl
"set tags+=~/.vim/tags/qt4

" OmniCppComplete
"let OmniCpp_NamespaceSearch = 1
"let OmniCpp_GlobalScopeSearch = 1
"let OmniCpp_ShowAccess = 1
"let OmniCpp_MayCompleteDot = 1
"let OmniCpp_MayCompleteArrow = 1
"let OmniCpp_MayCompleteScope = 1
"let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]

" automatically open and close the popup menu / preview window
"au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
"set completeopt=menuone,menu,longest,preview
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: SnippetsEmu..............(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
"let g:snip_start_tag = "_\."
"let g:snip_end_tag = "\._"
"let g:snip_elem_delim = ":"
"let g:snip_set_textmate_cp = '1'  " Tab to expand snippets, not automatically.
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: fuzzyfinder_textmate.....(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
let g:fuzzy_ignore = '.o;.obj;.bak;.exe;.pyc;.pyo;.DS_Store;.db'
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: autocomplpop.............(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
" complete option
"set complete=.,w,b,u,t,k
"let g:AutoComplPop_CompleteOption = '.,w,b,u,t,k'
"set complete=.
let g:AutoComplPop_IgnoreCaseOption = 0
let g:AutoComplPop_BehaviorKeywordLength = 2
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------}}}
" |   C O N F I G S                                                           |{{{
" -----------------------------------------------------------------------------
" |   [CONFIG]: Specially for DeaDSouL                                        |{{{
" -----------------------------------------------------------------------------
" Vim filetype detection file for 'conky56ccrc' conky config file
au BufNewFile,BufRead conky56ccrc set filetype=conkyrc
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Auto reloading files once saved                               |{{{
" -----------------------------------------------------------------------------
"if has('autocmd')
"    " .vimrc
"    augroup reload_vimrc
"        autocmd!
"        autocmd! BufWritePost ~/.vimrc nested source %
"    augroup END
"
"    " .bashrc
"    augroup reload_bashrc
"        autocmd!
"        autocmd! BufWritePost ~/.bashrc nested source %
"    augroup END
"endif
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Misc Settings                                                 |{{{
" -----------------------------------------------------------------------------
syntax on                       " Turn on syntax highlighting
syntax enable
"set spell spelllang=en_us       " [s : next, [s : previous, z= : suggestions
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set number                      " Show line numbers
set relativenumber              " Show relative numbers
set matchpairs+=<:>
set vb t_vb=                    " Turn off the bell, this could be more annoying, but I'm not sure how
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Indenting                                                     |{{{
" -----------------------------------------------------------------------------
set tabstop=8       " show existing tab with 4 spaces width
set softtabstop=4
set shiftwidth=4    " when indenting with '>', use 4 spaces width
"set textwidth=79   " ensure your line length doesn’t go beyond 80 characters
set expandtab       " On pressing tab, insert 4 spaces
set autoindent      " Automatically set the indent of a new line (local to buffer)
set si              " smartindent (local to buffer)
"set smarttab        " Enable smart tab
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Folding                                                       |{{{
" -----------------------------------------------------------------------------
" Enable folding
set foldmethod=syntax   " fold based on syntax
"set foldmethod=indent   " fold based on indent
set foldnestmax=3       " 99, deepest fold is 3 levels
"set nofoldenable        " don't fold by default
autocmd FileType vim,zsh,bash setlocal foldmethod=marker
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: File Stuff                                                    |{{{
" -----------------------------------------------------------------------------
" To show current filetype use: set filetype
autocmd FileType html :set filetype=xhtml " we couldn't care less about html
set modeline
filetype plugin indent on
"set fileformat=unix
set encoding=utf-8
set list    " Display tabs and trailing spaces
set listchars=tab:▷⋅,trail:⋅,nbsp:⋅
" Set list Chars - for showing characters that are not normally displayed i.e. whitespace, tabs, EOL
"set listchars=trail:.,tab:>-,eol:$
"set nolist
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Scrollbars                                                    |{{{
" -----------------------------------------------------------------------------
set sidescrolloff=7 "2
set scrolloff=3
set sidescroll=1
set numberwidth=4
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Windows                                                       |{{{
" -----------------------------------------------------------------------------
set equalalways " Multiple windows, when created, are equal in size
set splitbelow splitright
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Cursor highlights                                             |{{{
" -----------------------------------------------------------------------------
set cursorline
"set cursorcolumn
set showmatch       " Show matching bracets when text indicator is over them
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Searching                                                     |{{{
" -----------------------------------------------------------------------------
set hlsearch    " highlight search
set incsearch   " incremental search, search as you type
set ignorecase  " Ignore case when searching
set smartcase   " Ignore case when searching lowercase
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Status Line                                                   |{{{
" -----------------------------------------------------------------------------
set showcmd         " Display incomplete commands
set showmode        " Show current mode down the bottom
set ruler           " Show the cursor position all the time
set cmdheight=2     " The commandbar height

" building statusline
set statusline =%#identifier#
set statusline+=[%f]    "tail of the filename
set statusline+=%*

"display a warning if fileformat isnt unix
set statusline+=%#warningmsg#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

"display a warning if file encoding isnt utf-8
set statusline+=%#warningmsg#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*

" display help
set statusline+=%h      "help file flag
set statusline+=%y      "filetype

"read only flag
set statusline+=%#identifier#
set statusline+=%r
set statusline+=%*

"modified flag
set statusline+=%#warningmsg#
set statusline+=%m
set statusline+=%*

"fugitive
set statusline+=%{fugitive#statusline()}

"display a warning if &et is wrong, or we have mixed-indenting
set statusline+=%#error#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*

set statusline+=%{StatuslineTrailingSpaceWarning()}

set statusline+=%{StatuslineLongLineWarning()}

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"display a warning if &paste is set
set statusline+=%#error#
set statusline+=%{&paste?'[paste]':''}
set statusline+=%*

set statusline+=%=      "left/right separator
set statusline+=%{StatuslineCurrentHighlight()}\ \ "current highlight
set statusline+=%c,     "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file
set laststatus=2

"recalculate the trailing whitespace warning when idle, and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

"return '[\s]' if trailing white space is detected
"return '' otherwise
function! StatuslineTrailingSpaceWarning()
    if !exists("b:statusline_trailing_space_warning")
        if !&modifiable
            let b:statusline_trailing_space_warning = ''
            return b:statusline_trailing_space_warning
        endif
        if search('\s\+$', 'nw') != 0
            let b:statusline_trailing_space_warning = '[\s]'
        else
            let b:statusline_trailing_space_warning = ''
        endif
    endif
    return b:statusline_trailing_space_warning
endfunction

"return the syntax highlight group under the cursor ''
function! StatuslineCurrentHighlight()
    let name = synIDattr(synID(line('.'),col('.'),1),'name')
    if name == ''
        return ''
    else
        return '[' . name . ']'
    endif
endfunction

" recalculate the tab warning flag when idle and after writing
autocmd cursorhold,bufwritepost * unlet! b:statusline_tab_warning

"return '[&et]' if &et is set wrong
"return '[mixed-indenting]' if spaces and tabs are used to indent
"return an empty string if everything is fine
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let b:statusline_tab_warning = ''
        if !&modifiable
            return b:statusline_tab_warning
        endif
        let tabs = search('^\t', 'nw') != 0
        "find spaces that arent used as alignment in the first indent column
        let spaces = search('^ \{' . &ts . ',}[^\t]', 'nw') != 0
        if tabs && spaces
            let b:statusline_tab_warning =  '[mixed-indenting]'
        elseif (spaces && !&et) || (tabs && &et)
            let b:statusline_tab_warning = '[&et]'
        endif
    endif
    return b:statusline_tab_warning
endfunction

"recalculate the long line warning when idle and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_long_line_warning

"return a warning for "long lines" where "long" is either &textwidth or 80 (if
"no &textwidth is set)
"
"return '' if no long lines
"return '[#x,my,$z] if long lines are found, were x is the number of long
"lines, y is the median length of the long lines and z is the length of the
"longest line
function! StatuslineLongLineWarning()
    if !exists("b:statusline_long_line_warning")
        if !&modifiable
            let b:statusline_long_line_warning = ''
            return b:statusline_long_line_warning
        endif
        let long_line_lens = s:LongLines()
        if len(long_line_lens) > 0
            let b:statusline_long_line_warning = "[" .
                \ '#' . len(long_line_lens) . "," .
                \ 'm' . s:Median(long_line_lens) . "," .
                \ '$' . max(long_line_lens) . "]"
        else
            let b:statusline_long_line_warning = ""
        endif
    endif
    return b:statusline_long_line_warning
endfunction

"return a list containing the lengths of the long lines in this buffer
function! s:LongLines()
    let threshold = (&tw ? &tw : 80)
    let spaces = repeat(" ", &ts)
    let line_lens = map(getline(1,'$'), 'len(substitute(v:val, "\\t", spaces, "g"))')
    return filter(line_lens, 'v:val > threshold')
endfunction

"find the median of the given array of numbers
function! s:Median(nums)
    let nums = sort(a:nums)
    let l = len(nums)
    if l % 2 == 1
        let i = (l-1) / 2
        return nums[i]
    else
        return (nums[l/2] + nums[(l/2)-1]) / 2
    endif
endfunction
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Line Wrapping                                                 |{{{
" -----------------------------------------------------------------------------
"set nowrap
set wrap        " Don't wrap lines
"set linebreak  " Wrap lines at convenient points
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Directories                                                   |{{{
" -----------------------------------------------------------------------------
set undodir=~/.vim/files/undo   " Setup undo location
set undofile    " enable backup

set colorcolumn=+1  " mark the ideal max text width


set backupdir=~/.vim/files/bkp  " Setup backup location
set backup  " enable backup

set directory=~/.vim/files/swap//   " Set Swap directory

autocmd BufEnter * lcd %:p:h    " Sets path to directory buffer was loaded from
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Inser New Line                                                |{{{
" -----------------------------------------------------------------------------
" don't continue comments when pushing o/O
set formatoptions-=o
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Sessions                                                      |{{{
" -----------------------------------------------------------------------------
" Sets what is saved when you save a session
set sessionoptions=blank,buffers,curdir,folds,help,resize,tabpages,winsize

" Hide buffers when not displayed
set hidden

" Solves: there is a pause when leaving insert mode
set ttimeoutlen=50

" store lots of :cmdline history (my default: 1000)
set history=9999
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Mouse                                                         |{{{
" -----------------------------------------------------------------------------
"behave xterm
"set selectmode=mouse
"set mouse=a " Enable the mouse
"if !has("nvim")
"    set ttymouse=xterm2
"endif
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Ruby stuff                                                    |{{{
" -----------------------------------------------------------------------------
"compiler ruby         " Enable compiler support for ruby
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: GUI Settings                                                  |{{{
" -----------------------------------------------------------------------------
" Nice themes:
"   Monokai         vividchalk      xxx
"   Hybrid          murphy          xxx
"   Gruvbox         twilight        xxx
"   jellybeans      ir_black        xxx
"   solarized       zenburn         xxx
if has('gui_running')
    set guioptions-=T       "remove toolbar
    "set guioptions-=m      "remove menu bar
    set background=dark     " Theme
    colorscheme solarized   " Theme
else
    "set t_Co=256            " Tell the term has 256 colors
    "colorscheme default
    "colorscheme jellybeans
    colorscheme zenburn
    "colorscheme jellygrass
    "colorscheme jellyx
endif
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------}}}
" |   M A P P I N G S                                                         |{{{
" -----------------------------------------------------------------------------
" |   [PLUGIN]: Vim-Plug.................(junegunn/vim-plug)                  |{{{
" Shortcut : update plugins
:noremap ,u :PlugUpdate<CR>
" Shortcut : Upgrade vim-plug
:noremap ,U :PlugUpgrade<CR>
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: NERDTree.................(scrooloose/nerdTree)                |{{{
" Shortcut : toggle NERDTree
map <F9> :NERDTreeToggle<CR>
:noremap ,n :NERDTreeToggle<CR>
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: TagBar...................(preservim/tagbar)                   |{{{
nmap <F8> :TagbarToggle<CR>
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: CtrlP.vim................(kien/ctrlp.vim)                     |{{{
" -----------------------------------------------------------------------------
" * Press <F5> to purge the cache for the current directory to get new 
"   files, remove deleted files and apply new ignore options.
" * Press <c-f> and <c-b> to cycle between modes.
" * Press <c-d> to switch to filename only search instead of full path.
" * Press <c-r> to switch to regexp mode.
" * Use <c-j>, <c-k> or the arrow keys to navigate the result list.
" * Use <c-t> or <c-v>, <c-x> to open the selected entry in a new tab or 
"   in a new split.
" * Use <c-n>, <c-p> to select the next/previous string in the prompt's 
"   history.
" * Use <c-y> to create a new file and its parent directories.
" * Use <c-z> to mark/unmark multiple files and <c-o> to open them.
"
" * Press <C+S-p> or <c-[> to search in buffers.
"
" * Submit two dots '..' to go upward the directory tree by 1 level. To go
"   up multiple levels, use one extra dot for each extra level:
"   Raw input    Interpreted as
"   ..           ../
"   ...          ../../
"   ....         ../../../
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: vim-flake8...............(nvie/vim-flake8)                    |{{{
" -----------------------------------------------------------------------------
" <F7> to check syntax
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: JEDI-Vim.................(davidhalter/jedi-vim)               |{{{
let g:jedi#documentation_command = "K"
let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = "<leader>g"
"let g:jedi#goto_definitions_command = ""
let g:jedi#usages_command = "<leader>n"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#rename_command = "<leader>r"
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: vim-surround.............(tpope/vim-surround)                 |{{{
" COMMAND           CHANGE FROM             CHANGE TO
" ----------------  ----------------------  ----------------------
" cs"'              "Hello world!"          'Hello world!'
" cs'<p>            'Hello world!'          <p>Hello world!</p>
" cst"              <p>Hello world!</p>     "Hello world!"
" ds"               "Hello world!"          Hello world!
" ysiw]             Hello world!            [Hello] world!
" cs]{              [Hello] world!          { Hello } world!
" yssb OR yss)      { Hello } world!        ({ Hello } world!)
" ds{ds)            ({ Hello } world!)      Hello world!
" ysiw<em>          Hello world!            <em>Hello</em> world!
"
" Finally, let's try out visual mode. Press a capital V
" (for linewise visual mode) followed by S<p class="important">.
"
" <p class="important">
"   <em>Hello</em> world!
" </p>
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: vim-commentary...........(tpope/vim-commentary)               |{{{
" Use 
"   gcc     to comment out a line (takes a count),
"   gc      to comment out the target of a motion
"   gcap    to comment out a paragraph
"           gc in visual mode to comment out the selection, and
"           gc in operator pending mode to target a comment.
"           You can also use it as a command, either with a range
"           like :7,17Commentary, or as part of a :global invocation
"           like with :g/TODO/Commentary. That's it.
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: markdown-preview.nvim....(iamcco/markdown-preview.nvim)       |{{{
" normal/insert
" <Plug>MarkdownPreview
" <Plug>MarkdownPreviewStop
" <Plug>MarkdownPreviewToggle
" 
" " example
"nmap <C-s> <Plug>MarkdownPreview
"nmap <M-s> <Plug>MarkdownPreviewStop
"nmap <C-p> <Plug>MarkdownPreviewToggle
" -----------------------------------------------------------------------------}}}
" |   [PLUGIN]: WindowSwap.vim...........(wesQ3/vim-windowswap)               |{{{
nnoremap <silent> <leader>yw :call WindowSwap#MarkWindowSwap()<CR>
map <F4> :call WindowSwap#MarkWindowSwap()<CR>
"noremap <F4> :call WindowSwap#MarkWindowSwap()<CR>

nnoremap <silent> <leader>pw :call WindowSwap#DoWindowSwap()<CR>
map <C-F4> :call WindowSwap#DoWindowSwap()<CR>
"noremap <C-F4> :call WindowSwap#DoWindowSwap()<CR>

nnoremap <silent> <leader>ww :call WindowSwap#EasyWindowSwap()<CR>
map <S-F4> :call WindowSwap#EasyWindowSwap()<CR>
"noremap <S-F4> :call WindowSwap#EasyWindowSwap()<CR>
" -----------------------------------------------------------------------------}}}
"
" | - [PLUGIN]: LimeLight.vim............(junegunn/limelight.vim)             |{{{
" -----------------------------------------------------------------------------
"nmap <Leader>l <Plug>(Limelight)
"xmap <Leader>l <Plug>(Limelight)
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: OmniCppComplete..........(Unavailable)                        |{{{
" -----------------------------------------------------------------------------
" Build tags of your own project with CTRL+F12
"map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
"noremap <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
"inoremap <F12> <Esc>:!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: SnippetsEmu..............(Unavailable)                        |{{{
"imap <unique> <C-j> <Plug>Jumper
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: fuzzyfinder_textmate.....(Unavailable)                        |{{{
"map ,f :FuzzyFinderTextMate<CR>
"map ,b :FuzzyFinderBuffer<CR>
" -----------------------------------------------------------------------------}}}
" | - [PLUGIN]: vim-instant-markdown.....(suan/vim-instant-markdown)          |{{{
"map <leader>md :InstantMarkdownPreview<CR>
" -----------------------------------------------------------------------------}}}
"
" |   [CONFIG]: Helpers                                                       |{{{
nnoremap Y y$   "make Y consistent with C and D
set pastetoggle=<F2>    " toggle paste-mode
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Cursor Movement                                               |{{{
" Professor VIM says '87% of users prefer jj over esc', jj abrams strongly disagrees
imap jj <Esc>

" Make cursor move by visual lines instead of file lines (when wrapping)
map <up> gk
map k gk
imap <up> <C-o>gk
map <down> gj
map j gj
imap <down> <C-o>gj
map E ge

" awesome, inserts new line without going into insert mode
map <S-Enter> O<ESC>
map <Enter> o<ESC>
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: Windows                                                       |{{{
" Shortcuts : Vertical split then hop to new buffer
:noremap ,v :vsp^M^W^W<cr>
:noremap ,h :split^M^W^W<cr>

" Split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: For Ruby                                                      |{{{
" Ruby compiler
"map <F5> :!ruby %<CR>
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: For Pythons                                                   |{{{
"imap ,c class 
"imap ,d def 
autocmd FileType python imap <buffer> ,c class 
autocmd FileType python imap <buffer> ,d def 
autocmd FileType python imap <buffer> ,S self
autocmd FileType python imap <buffer> ,s self.
autocmd FileType python imap <buffer> ,i import 
autocmd FileType python imap <buffer> ,f from 
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: For PHP                                                       |{{{
imap ,f function 
"imap ,t $this->
"imap uu _
"imap hh =>
"imap kk ->
autocmd FileType php imap <buffer> ,f function 
autocmd FileType php imap <buffer> ,t $this->
autocmd FileType php imap <buffer> uu _
autocmd FileType php imap <buffer> hh => 
autocmd FileType php imap <buffer> kk ->
" -----------------------------------------------------------------------------}}}
" |   [CONFIG]: For HTML                                                      |{{{
" Autocommand for HTML files
"autocmd FileType html nnoremap <buffer> ,d :silent !dolphin . &<CR>
autocmd FileType html nnoremap <buffer> ,d :!dolphin . &<CR>
autocmd FileType html nnoremap <buffer> ,b :!brave-browser-stable %<CR>
autocmd FileType html nnoremap <buffer> ,B :!brave-browser-stable --incognito %<CR>
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------}}}
" -----------------------------------------------------------------------------
